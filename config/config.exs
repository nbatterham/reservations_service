# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :reservations_service,
  ecto_repos: [ReservationsService.Repo]

# Configures the endpoint
config :reservations_service, ReservationsService.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "U1cD52Qh7Hw7QlPyl7PclnOxG2TBpSoA15Tfede2LUbnu48oa+VLmYGnvLF9jmWw",
  render_errors: [view: ReservationsService.ErrorView, accepts: ~w(json)],
  pubsub: [name: ReservationsService.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
