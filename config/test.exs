use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :reservations_service, ReservationsService.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :reservations_service, ReservationsService.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "reservations_service_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
