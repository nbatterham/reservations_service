defmodule ReservationsService.Repo.Migrations.CreateResource do
  use Ecto.Migration

  def change do
    create table(:resources) do
      add :name, :string
      add :description, :string
      add :barcode, :string

      timestamps()
    end

  end
end
