defmodule ListingResourcesIntegrationTest do
  use ExUnit.Case, async: true
  use ReservationsService.IntegrationCase
  use Plug.Test
  alias ReservationsService.Router
  alias ReservationsService.Resource
  alias ReservationsService.Repo


  @opts Router.init([])
  test 'listing resources' do
    %Resource{
      barcode: "14654234514246",
      name: "room 201",
      description: "meeting room, capacity 5"}
      |> Repo.insert!

    resources = Repo.all(Resource)
              |> Poison.encode!

    conn = conn(:get, "/v1/resources")
    response = Router.call(conn, @opts)
    assert response.status === 200
    assert response.resp_body == resources
  end
end
