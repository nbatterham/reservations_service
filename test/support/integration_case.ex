defmodule ReservationsService.IntegrationCase do
  use ExUnit.CaseTemplate

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(ReservationsService.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Watchlist, {:shared, self()})
    end

    :ok
  end
end
