defmodule ReservationsService.ResourcesController do
  use ReservationsService.Web, :controller
  use PhoenixSwagger

  alias ReservationsService.Resource

  def swagger_definitions do
    %{
      Resource: swagger_schema do
        title "Resource"
        description "A resource that can be booked"
        properties do
          id :integer, "Resource ID"
          name :string, "Name of the resource", required: true
          description :string, "Description of the resource", required: true
          inserted_at :string, "Creation timestamp", format: :datetime
          updated_at :string, "Update timestamp", format: :datetime
        end
        example %{
          id: 1,
          name: "Room 201",
          description: "Meeting room, capacity 5",
          barcode: "2315853492524534"
        }
      end,
      ResourceRequest: swagger_schema do
        title "ResourceRequest"
        description "POST body for creating a resource"
        property :resource, Schema.ref(:Resource), "The resource details"
      end,
      ResourceResponse: swagger_schema do
        title "ResourceResponse"
        description "Response schema for single resource"
        property :data, Schema.ref(:Resource), "The resource details"
      end,
      ResourcesResponse: swagger_schema do
        title "ResourcesReponse"
        description "Response schema for multiple Resources"
        property :data, Schema.array(:Resource), "The resource details"
      end
    }
  end

  swagger_path(:index) do
    get "/v1/resources"
    summary "List of all resources"
    description "List of all resources"
    produces "application/json"
    response 200, "OK", Schema.ref(:ResourcesResponse)
  end
  def index(conn, _params) do
    resources = Repo.all(Resource)
    render(conn, "index.json", resources: resources)
  end

  swagger_path(:show) do
    get "/v1/resources/{id}"
    summary "Show Resource"
    description "Show a resource by ID"
    produces "application/json"
    parameter :id, :path, :integer, "Resource ID", required: true, example: 1
    response 200, "OK", Schema.ref(:ResourceResponse), example: %{
      data: %{
        id: 1, name: "Room 201", description: "Meeting room, capacity 5", barcode: "2154253423542"
      }
    }
  end
  def show(conn, %{"id" => id}) do
    resource = Repo.get!(Resource, id)
    render(conn, "show.json", resource: resource)
  end

  swagger_path(:create) do
    post "/v1/resources"
    summary "Create a resource"
    description "Creates a new bookable resource"
    consumes "application/json"
    produces "application/json"
    parameter :resource, :body, Schema.ref(:ResourceRequest), "The resource details", example: %{
      resource: %{name: "Room 201", description: "Meeting room, capacity 5", barcode: "2154253423542"}
    }
    response 201, "Resource created OK", Schema.ref(:ResourceResponse), example: %{
      data: %{
        id: 1, name: "Room 201", description: "Meeting room, capacity 5", barcode: "2154253423542", inserted_at: "2017-02-08T12:34:55Z", updated_at: "2017-02-12T13:45:23Z"
      }
    }
  end
  def create(conn, %{"resource" => resource_params}) do
    changeset = Resource.changeset(%Resource{}, resource_params)

    case Repo.insert(changeset) do
      {:ok, resource} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", resources_path(conn, :show, resource))
        |> render("show.json", resource: resource)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(ReservationsService.ChagesetView, "error.json", changeset: changeset)
    end
  end

  swagger_path(:delete) do
    delete "/v1/resources/{id}"
    summary "Delete a resource"
    description "Delete a resource by ID"
    parameter :id, :path, :integer, "Resource ID", required: true, example: 3
    response 204, "No Content - Deleted Successfully"
  end
  def delete(conn, %{"id" => id}) do
    resource = Repo.get!(Resource, id)
    Repo.delete!(resource)
    send_resp(conn, :no_content, "")
  end
end
