defmodule ReservationsService.RedirectController do
  use ReservationsService.Web, :controller
  @send_to "/v1/docs/"

  def redirector(conn, _params), do: redirect(conn, to: @send_to)

end
