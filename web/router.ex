defmodule ReservationsService.Router do
  use ReservationsService.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ReservationsService do
    pipe_through :api

    get "/", RedirectController, :redirector
  end

  scope "/v1", ReservationsService do
    pipe_through :api

    get "/resources", ResourcesController, :index
    post "/resources", ResourcesController, :create

    get "/resources/:id", ResourcesController, :show
    delete "/resources/:id", ResourcesController, :delete
  end

  scope "/v1/docs" do
    forward "/", PhoenixSwagger.Plug.SwaggerUI, otp_app: :reservations_service, swagger_file: "swagger.json"
  end

  def swagger_info do
    %{
      info: %{
        version: "1.0",
        title: "Reservations Service"
      },
      host: "localhost:4000",
    }
  end
end
