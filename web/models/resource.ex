defmodule ReservationsService.Resource do
  use ReservationsService.Web, :model

  schema "resources" do
    field :name, :string
    field :description, :string
    field :barcode, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :description, :barcode])
    |> validate_required([:name, :description, :barcode])
  end

  defimpl Poison.Encoder, for: ReservationsService.Resource do
    def encode(resource, _options) do
      resource
      |> Map.from_struct
      |> Map.drop([:__meta__, :__struct])
      |> Poison.encode!
    end
  end
end
