defmodule ReservationsService.ResourcesView do
  use ReservationsService.Web, :view

    def render("index.json", %{resources: resources}) do
      %{data: render_many(resources, ReservationsService.ResourcesView, "resource.json")}
    end

    def render("show.json", %{resource: resource}) do
      %{data: render_one(resource, ReservationsService.ResourcesView, "resource.json")}
    end

    def render("resource.json", %{resources: resources}) do
      %{id: resources.id,
        name: resources.name,
        description: resources.description,
        barcode: resources.barcode}
    end
end
